# gpack: a package manager for GJS

## Features

* Automatic gresource/meson handling!

`gpack` bundles all of its dependencies into a separate gresource bundle alongside your application!

## Setup

1. Create a `gpack.json` file in your project root. Replace `$prefix` with your package prefix.

```json
{ "gresource:prefix": "$prefix" }
```

2. Run `gpack --all`
3. Add the `src/gpack.js` file to your project.
4. Add `subdir('gpack')` to your root meson.build.
5. Call `imports.gpack.load();` *after* `imports.package.init` and *before* `imports.package.run`.

## Usage

Install a package without adding to dependencies:

> gpack --install [name]

Intall a package, adding to dependency list:

> gpack --add [name]

Install entire dependency list:

> gpack --all

## Where Are The Files?

All dependencies are stored in the `gpack/` folder relative to the `gpack.json` file in your project root directory. `gpack.json` contains a list of all your dependencies and their versions.