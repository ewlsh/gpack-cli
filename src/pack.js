const { GLib, Gio } = imports.gi;

const { generateResourceXML } = imports.xml;
const { generateMesonBuild } = imports.meson;

const { delete_recursive_async } = imports.delete_recursive.delete_recursive;

const ByteArray = imports.byteArray;

// HTTPS File Promisification
const _HttpsFilePrototype = Gio.File.new_for_uri('http://').constructor.prototype;
Gio._promisify(_HttpsFilePrototype, "load_contents_async", "load_contents_finish");

// Local File Promisification
Gio._promisify(Gio._LocalFilePrototype, "create_async", "create_finish");
Gio._promisify(Gio._LocalFilePrototype, "load_contents_async", "load_contents_finish");
Gio._promisify(Gio._LocalFilePrototype, "replace_contents_bytes_async", "replace_contents_finish");
Gio._promisify(Gio._LocalFilePrototype, "enumerate_children_async", "enumerate_children_finish");
Gio._promisify(Gio._LocalFilePrototype, "delete_async", "delete_finish");

const _LocalFileEnumeratorPrototype = Gio.File.new_for_path("/").enumerate_children("", Gio.FileQueryInfoFlags.NOFOLLOW_SYMLINKS, null).constructor.prototype;
Gio._promisify(_LocalFileEnumeratorPrototype, "next_files_async", "next_files_finish");

async function loadFile(path, default_contents) {
    const file = typeof path === 'string' ? Gio.File.new_for_path(path) : path;

    try {

        const [content] = await file.load_contents_async(null);

        const res = ByteArray.toString(content, 'utf-8');

        return res;
    } catch (err) {
        if (!err.matches(Gio.IOErrorEnum, Gio.IOErrorEnum.NOT_FOUND)) {
            log(`Failed to save default contents: ${err}`);
            log(err.stack);
            throw err;
        } else if (default_contents) {
            log(`Saving default... ${default_contents}`);
            await saveFile(file, default_contents);
        }
    }

}

async function saveFile(path, contents) {
    const file = typeof path === 'string' ? Gio.File.new_for_path(path) : path;

    try {
        if (contents !== "") {
            await file.replace_contents_bytes_async(ByteArray.fromString(contents, 'utf-8'), null, false, Gio.FileCreateFlags.NONE, null);
        } else {
            await file.create_async(Gio.FileCreateFlags.NONE, 0, null);
        }
    } catch (err) {
        throw err;
    }
}

async function downloadFile(url) {
    log(`Downloading File: ${url}`);
    try {
        const file = Gio.File.new_for_uri(url);

        const [content] = await file.load_contents_async(null);

        return ByteArray.toString(content, 'utf-8');
    } catch (err) {
        log(`Failed to download file: ${err}`);

        return null;
    }
}

function resolveRepo(repo_type) {
    switch (repo_type) {
        case 'github':
            return ({ owner, name, version }) => (file) => `https://raw.githubusercontent.com/${owner}/${name}/${version}/${file}`;
        case 'gitlab':
            return ({ owner, name, version }) => (file) => `https://gitlab.com/${owner}/${name}/-/raw/${version}/${file}`
        case 'gnome':
            return ({ owner, name, version }) => (file) => `https://gitlab.gnome.org/${owner}/${name}/-/raw/${version}/${file}`
    }
}

const GPACK_URL = (name, version = 'master') => `https://registry.gpack.app/x/${name}@${version}`;

async function loadGPackData(gpack_json) {
    const raw = await loadFile(gpack_json, `{}`);

    const json = JSON.parse(raw);

    return Object.assign({ dependencies: {} }, json);
}

async function saveDependencies(gpack_json, dependencies) {
    const json = await loadGPackData(gpack_json);

    json.dependencies = dependencies;

    await saveFile(gpack_json, JSON.stringify(json, null, 4));
}

async function create_package_file(base, package_name, file_path, content) {
    try {
        const intended_file = Gio.File.new_for_path(`${base}/${package_name}/${file_path}`);
        const parent_dir = intended_file.get_parent();

        try {
            if (parent_dir && !parent_dir.make_directory_with_parents(null)) {
                throw new Error(`Failed to create directories for: ${file_path}`)
            }
        } catch (err) {
            if (!err.matches(Gio.IOErrorEnum, Gio.IOErrorEnum.EXISTS)) {
                log(err);
                log(err.stack);
            }
        }

        log(`Writing ${file_path} to ${intended_file.get_path()}`);

        await saveFile(intended_file, content);

        return `${package_name}/${file_path}`;
    } catch (err) {
        log(`Error occurred while creating package file: ${err}`);
    }
}


var GPacker = class GPacker {
    constructor() {
        this._baseDir = GLib.get_current_dir();
        this._gpackDir = `${this._baseDir}/gpack`;

        // TODO Make async
        Gio.File.new_for_path(this._gpackDir).make_directory(null);

        this._gpack = null;

        this._filesMap = new Map();
    }

    _getGPackFile() {
        const gpack_json = `${this._baseDir}/gpack.json`;
        return Gio.File.new_for_path(gpack_json);
    }

    async async_init() {
        this._gpackFile = this._getGPackFile();
        this._gpack = await loadGPackData(this._gpackFile);

        await Promise.all(Object.keys(this._gpack.dependencies).map(async dep => {
            try {
                const pack_path = `${dep}/pack.json`;
                const absolute_path = `${this._gpackDir}/${pack_path}`;
                const raw = await loadFile(absolute_path);
                const pack = JSON.parse(raw);

                this._filesMap.set(dep, pack.files.map(file => `${dep}/${file}`));
            } catch (err) { }
        }));
    }

    async post_op() {
        const allFiles = Array.from(this._filesMap.values()).reduce((prev, next) => {
            prev.push(...next);
            return prev;
        }, []);

        const prefix = this._gpack["gresource:prefix"];
        const xml = generateResourceXML(allFiles, prefix.replace(/\./g, '/'));
        await saveFile(`${this._gpackDir}/${prefix}.gpack.gresource.xml`, xml);

        const mesonBuild = generateMesonBuild(prefix);
        await saveFile(`${this._gpackDir}/meson.build`, mesonBuild);
    }

    async _install(base, package_name, version = 'master') {
        log(`Installing ${package_name} in ${base}`);
        const package_url = GPACK_URL(package_name);
        log(`Using package url: ${package_url}`);
        const raw_index = await downloadFile(package_url);

        if (raw_index === null) {
            log('No package found.');
            return false;
        }

        const package_dir = await Gio.File.new_for_path(`${base}/${package_name}`);

        try {
            await delete_recursive_async(package_dir);
        } catch (err) {
            if (!err.matches(Gio.IOErrorEnum, Gio.IOErrorEnum.NOT_FOUND)) {
                log(err);
                log(err.stack);
            }
        }


        const index = JSON.parse(raw_index);
        const name = index.name;
        const owner = index.owner;
        const repoInfo = resolveRepo(index.repo);
        const repoFile = repoInfo({ name, owner, version });

        const pack_uri = repoFile("pack.json");
        const pack_raw = await downloadFile(pack_uri);
        const pack = JSON.parse(pack_raw);

        await create_package_file(base, package_name, "pack.json", pack_raw);

        const files = pack.files;

        const resolvedFiles = files.map(file => [file, repoFile(file)]);
        const downloadedFiles = await Promise.all(resolvedFiles.map(async ([file, rf]) => {
            const contents = await downloadFile(rf);
            return [file, contents];
        }));

        const paths = await Promise.all(downloadedFiles.map(([file, content]) => create_package_file(base, package_name, file, content)));

        this._filesMap.set(package_name, [...paths]);

        return true;
    }

    async install(package_name, version = 'master') {
        try {
            if (await this._install(this._gpackDir, package_name, version)) {
                await this.post_op();
                return true;
            }

            return false;
        } catch (err) {
            log(`Error occurred during installation: ${err}`);
        }
    }

    async add(package_name, version = 'master') {
        try {
            const { dependencies } = this._gpack;

            if (package_name in dependencies && dependencies[package_name] !== version) {
                log(`Replacing version ${dependencies[package_name]} with ${version}`);
            }

            dependencies[package_name] = version;

            if (await this._install(this._gpackDir, package_name, version)) {
                log(`Added ${package_name}!`);

                await saveDependencies(this._gpackFile, dependencies);

                await this.post_op();

                return true;
            }

            return false;
        } catch (err) {
            log(`Failed to add package ${package_name}: ${err}`);
            log(err.stack);
        }
    }

    async install_all() {
        try {
            const { dependencies } = this._gpack;

            await Promise.all(Object.entries(dependencies).map(([name, version]) => this.install(name, version)));

            log("Installed all packages!");

            await this.post_op();
        } catch (err) {
            log(`Failed to install all packages: ${err}`);
        }
    }
}
