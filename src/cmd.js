const { GLib } = imports.gi;

const { GPacker } = imports.pack;

const ml = GLib.MainLoop.new(null, false);

async function handle(argv) {
    const cmd = argv[0];
    const arg = argv[1];

    const packer = new GPacker();

    await packer.async_init();

    try {
        if (cmd == '--install') {
            await packer.install(arg);
        } else if (cmd == '--all') {
            await packer.install_all();
        } else if (cmd == '--add') {
            await packer.add(arg);
        } else if (cmd == '--help') {
            // TODO Print this *not* as logs.
            log(`gpack help:`);
            log(`--add       Install and add a package`);
            log(`--all       Install all added packages`);
            log(`--install   Install a package without adding to dependencies.`);
            log(`--help      Help`);
        } else {
            log("No such command! Use --help for help.");
        }
    } catch (err) {
        log(err);
        log(err.stack);
    }
}

function run(argv) {
    handle(argv)
        .then(() => log('Done!'))
        .catch(err => {
            log(err);
            log(err.stack);
        })
        .finally(() => ml.quit());

    ml.run();
}
