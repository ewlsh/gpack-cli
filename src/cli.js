log("Welcome to gpack: the GJS packaging utility.");

pkg.require({
    'Gio': '2.0',
    'GLib': '2.0'
});

const { GLib, Gio } = imports.gi;

function main() {
    imports.cmd.run(ARGV);
}
