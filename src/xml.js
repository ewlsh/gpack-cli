function generateResourceXML(files, prefix) {
    return `<?xml version="1.0" encoding="UTF-8"?>
<gresources>
    <gresource prefix="/${prefix}/gpack">
        ${files.map(file => `<file>${file}</file>`).join(`\n        `)}
    </gresource>
</gresources>`
}