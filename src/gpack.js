const { Gio, GLib } = imports.gi;

function load(pack = pkg) {
    let { name, pkgdatadir } = pack;
    let { _runningFromSource, _runningFromMesonSource, _makeNamePath } = pkg;

    let resource_dir = "";

    if (_runningFromSource()) {
        log('gpack: running from source tree, using local files!');

        // Running from source directory
        resource_dir = GLib.build_filenamev([_base, 'gpack']);
    } else if (_runningFromMesonSource()) {
        log('gpack: running from Meson, using local files!');
        let bld = GLib.getenv('MESON_BUILD_ROOT');

        try {
            let resource = Gio.Resource.load(GLib.build_filenamev([bld, 'gpack',
                `${name}.gpack.gresource`]));
            resource._register();
            resource_dir = `resource://${_makeNamePath(name)}/gpack`;
        } catch (e) {
            resource_dir = GLib.build_filenamev([bld, 'gpack']);
        }
    } else {
        try {
            let resource = Gio.Resource.load(GLib.build_filenamev([pkgdatadir,
                `${name}.gpack.gresource`]));
            resource._register();

            resource_dir = `resource://${_makeNamePath(name)}/gpack`;
        } catch (err) {
            log(`Failed to load gpack: ${err}`);
            log(err.stack);
        }
    }

    imports.searchPath.unshift(resource_dir);
}