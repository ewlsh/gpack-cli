function generateMesonBuild(prefix) {
    return `pkgdatadir = join_paths(get_option('datadir'), meson.project_name())
gnome = import('gnome')

gpack_res = gnome.compile_resources('${prefix}.gpack',
  '${prefix}.gpack.gresource.xml',
  gresource_bundle: true,
  install: true,
  install_dir: pkgdatadir,
)`
}